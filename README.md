
- These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

- [Pre-requisites]
  - [Docker]

## Docker

For development we're using docker with node 13.

## Developing

The port of this appplication is predefined as `3009`

## Getting Started

### Installing
```
    docker-compose run app ash
    npm install
```

Exit the container by typing:

```
    exit
```

## Running

### Start the App

```
    docker-compose up app
```

### Run Tests

```
    docker-compose run tst
```

## Routes

** FETCH GAME BY ID **

*GET* ```/users```

*200* ```OK```

*Example*
```
[
    {
        "id": 1,
        "name": "Leanne Graham",
        "username": "Bret",
        "address": {
            "street": "Kulas Light",
            "suite": "Apt. 556",
            "city": "Gwenborough",
            "zipcode": "92998-3874",
        },
        "phone": "1-770-736-8031 x56442",
        "website": "hildegard.org",
        "company": {
            "name": "Romaguera-Crona",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
        },
        "posts": [
            {
                "userId": 1,
                "id": 1,
                "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
            },
            {
                "userId": 1,
                "id": 2,
                "title": "qui est esse",
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
            },
        ]
  },
]
```



