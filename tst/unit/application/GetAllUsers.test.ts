import {
    IUser,
    IPost,
} from 'src/core/IUser';
import GetAllUsers from 'src/application/GetAllUsers';
import UsersMock from 'tst/support/Users';
import PostsMock from 'tst/support/Posts';

describe('APPLICATION :: GetAllUsers', () => {
    describe('Should all users ', () => {
        const mockJsonPlaceHolderService = {
            fetchPosts: (): IPost[] => PostsMock,
            fetchUsers: (): IUser[] => UsersMock,
        }

        const getAllUsers = new GetAllUsers({ jsonPlaceHolderService: mockJsonPlaceHolderService });
        
        test('Users by company name', async () => {
            const conditions = {
                company: {
                    name: 'Romaguera-Crona'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);

            expect(users[0].id).toBe(1);
            expect(users[0].name).toBe('Leanne Graham');
            expect(users[0].company.name).toBe('Romaguera-Crona');
            expect(users[0].company.catchPhrase).toBe('Multi-layered client-server neural-net');
            expect(users[0].company.bs).toBe('harness real-time e-markets');
            expect(users[0].posts).toHaveLength(2);
        });

        test('Users by company catchPhrase', async () => {
            const conditions = {
                company: {
                    catchPhrase: 'Proactive didactic contingency'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);
            expect(users[0].id).toBe(2);
            expect(users[0].name).toBe('Ervin Howell');
            expect(users[0].company.name).toBe('Deckow-Crist');
            expect(users[0].company.catchPhrase).toBe('Proactive didactic contingency');
            expect(users[0].company.bs).toBe('synergize scalable supply-chains');
            expect(users[0].posts).toHaveLength(1);
        });


        test('Users by company bs', async () => {
            const conditions = {
                company: {
                    bs: 'e-enable strategic applications'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);
            expect(users[0].id).toBe(3);
            expect(users[0].name).toBe('Clementine Bauch');
            expect(users[0].company.name).toBe('Romaguera-Jacobson');
            expect(users[0].company.catchPhrase).toBe('Face to face bifurcated interface');
            expect(users[0].company.bs).toBe('e-enable strategic applications');
            expect(users[0].posts).toHaveLength(1);
        });
    });

    describe('Should empty user', () => {
        const mockJsonPlaceHolderService = {
            fetchPosts: (): IPost[] => PostsMock,
            fetchUsers: (): IUser[] => UsersMock,
        }

        const getAllUsers = new GetAllUsers({ jsonPlaceHolderService: mockJsonPlaceHolderService });
        
        test('Users by company name', async () => {
            const conditions = {
                company: {
                    name: 'Romaguera-Cronaasasasasasasas'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);

            expect(users).toHaveLength(0);
        });

        test('Users by company catchPhrase', async () => {
            const conditions = {
                company: {
                    catchPhrase: 'Proactive didactic contingencyASFSAFASF'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);
            expect(users).toHaveLength(0);
        });


        test('Users by company bs', async () => {
            const conditions = {
                company: {
                    bs: 'e-enable strategic applicationsasfsafsafsafasf'
                },
            };
            const users: IUser[] = await getAllUsers.execute(conditions);
            expect(users).toHaveLength(0);
        });
    });
});