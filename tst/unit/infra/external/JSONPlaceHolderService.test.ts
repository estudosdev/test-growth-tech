import {
    IUser,
    IPost,
} from 'src/core/IUser';
import UsersMock from 'tst/support/Users';
import PostsMock from 'tst/support/Posts';
import JsonPlaceHolderService from 'src/infra/external/JSONPlaceHolderService';

describe('APPLICATION :: GetAllUsers', () => {
    let createMockHttpClient;
    beforeAll(() => {
        createMockHttpClient = (type: string): any => {
            switch(type) {
                case 'User':
                    return {
                        get: (_path:string): { data: IUser[] } => ({
                            data: UsersMock,
                        }),
                    }
                case 'Post':
                    return {
                        get: (_path:string): { data: IPost[] } => ({
                            data: PostsMock
                        }),
                    }
                default:
                    break;
            }
        };
    });

    test('Should all Users ', async () => {
        const mockHttpClient = createMockHttpClient('User');
        const jsonPlaceHolderService = new JsonPlaceHolderService({ httpClient: mockHttpClient });
        const users: IUser[] = await jsonPlaceHolderService.fetchUsers();
        expect(users).toHaveLength(UsersMock.length);
    });

    test('Should all Posts ', async () => {
        const mockHttpClient = createMockHttpClient('Post');
        const jsonPlaceHolderService = new JsonPlaceHolderService({ httpClient: mockHttpClient });
        const posts: IPost[] = await jsonPlaceHolderService.fetchPosts();
        expect(posts).toHaveLength(PostsMock.length);
    });
});