import IServer from 'src/interface/IServer';

class Application {
    private httpServer: IServer;
    
    public constructor({ httpServer }) {
        this.httpServer = httpServer;
    }

    public async start(): Promise<void> {
        try {
            await this.httpServer.start();
            console.log('App is running');
        } catch (error) {
            throw error;
        }
    }
}

export default Application;
