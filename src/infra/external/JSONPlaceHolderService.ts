import IHttpClient from 'src/infra/httpClient/IHttpClient';
import { IUser, IPost } from 'src/core/IUser';

class JsonPlaceHolderService {
    private httpClient: IHttpClient;

    constructor({ httpClient }) {
        this.httpClient = httpClient;
    }

    public async fetchUsers(): Promise<IUser[]> {
        const { data } = await this.httpClient.get('/users');
        return data;
    }

    public async fetchPosts(): Promise<IPost[]> {
        const { data } = await this.httpClient.get(`/posts`);
        return data;
    }
}

export default JsonPlaceHolderService;