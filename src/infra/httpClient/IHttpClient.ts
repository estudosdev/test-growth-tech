interface IHttpClient {
    get: (...params) => Promise<any>;
};

export default IHttpClient;