import axios, { AxiosInstance } from 'axios';
import IHttpClient from './IHttpClient';

class AdaptorAxios implements IHttpClient {
    private instance: AxiosInstance;

    constructor(){
        this.createInstance();
    }

    createInstance() {
        this.instance = axios.create({
            baseURL: 'http://jsonplaceholder.typicode.com',
        });
    }

    async get(path: string) {
        return await this.instance.get(path);
    }
}

export default AdaptorAxios;