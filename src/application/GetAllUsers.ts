import IApplication from './IApplication';
import { IUser, IPost } from 'src/core/IUser';


class GetAllUsers implements IApplication {
    private jsonPlaceHolderService: any;
    private posts: IPost[] = [];
    private users: IUser[] = [];

    constructor({ jsonPlaceHolderService }) {
        this.jsonPlaceHolderService = jsonPlaceHolderService;
    }

    private filterByCompany(user: IUser, conditions: any): boolean {
        const { company } = user;
        const { company: { name, catchPhrase, bs } } = conditions;
        
        if (name) {
            return company.name === name ? true : false;
        } else if(catchPhrase) {
            return company.catchPhrase === catchPhrase ? true : false
        } else if (bs) {
            return company.bs === bs ? true : false
        } else {
            return true
        }
    }

    private async fetchPosts(): Promise<void> {
        this.posts = await this.jsonPlaceHolderService.fetchPosts();
    }

    private async concatPostsWithUser(user: IUser[]): Promise<IUser[]> {
        return user.map(user => {
            const posts: IPost[] = this.posts.filter(post => post.userId === user.id);
            return Object.assign(user, { posts })
        });
    }

    private async fetchUsers(conditions): Promise<IUser[]> {
        this.users = await this.jsonPlaceHolderService.fetchUsers();
        if (conditions && conditions.company) {
            const usersFiltred: IUser[] = this.users.filter((user) => this.filterByCompany(user, conditions));

            return await this.concatPostsWithUser(usersFiltred);
        } else {
            return this.users;
        }
    }

    public async execute(conditions): Promise<IUser[]> {
        try {
            await this.fetchPosts();
            return await this.fetchUsers(conditions);
        } catch (error) {
            throw error;
        }
    }
};

export default GetAllUsers;