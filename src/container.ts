//TODO move to /src
import { createContainer, asClass, asFunction } from 'awilix';
import { UserRouter } from 'src/interface/net/http/router/index';

import Application from './Application';

import HttpRoutes from 'src/interface/net/http/Routes';
import HttpServer from 'src/interface/net/http/Server';
import { UserController } from 'src/interface/net/http/controller/index';

import AdaptorAxios from 'src/infra/httpClient/AdaptorAxios';
import JsonPlaceHolderService from 'src/infra/external/JSONPlaceHolderService';

import GetAllUsers from 'src/application/GetAllUsers';

const container = createContainer();

// Interface
container.register({
    userRouter: asFunction(UserRouter).singleton(),
    httpRoutes: asFunction(HttpRoutes).singleton(),
    httpServer: asClass(HttpServer).singleton(),
    userController: asClass(UserController).singleton(),
});

// Infra
container.register({
    httpClient: asClass(AdaptorAxios).singleton(),
    jsonPlaceHolderService: asClass(JsonPlaceHolderService).singleton(),
});

// Applications
container.register({
    getAllUsers: asClass(GetAllUsers),
})

// System
container.register({
    app: asClass(Application).singleton(),
});

export default container;
