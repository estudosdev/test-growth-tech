import { Request, Response } from 'express';
import HttpStatus from 'http-status';
import IApplication from 'src/application/IApplication';
import { IUser } from 'src/core/IUser';

class UserController {
    private getAllUsers: IApplication;

    constructor({ getAllUsers }) {
        this.getAllUsers = getAllUsers;
    }

    public async getAll(req: Request, res: Response): Promise<any> {
        const { query: conditions } = req;
        const users: IUser[] = await this.getAllUsers.execute(conditions);
        return res.status(HttpStatus.OK).json(users);
    }
}

export default UserController;
