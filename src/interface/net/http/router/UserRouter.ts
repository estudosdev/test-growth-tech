import { Router } from 'express';

export = ({ userController }): Router => {
    const router = Router();

    router.get('/', (req, res) => userController.getAll(req, res));

    return router;
};
