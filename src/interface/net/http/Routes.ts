import { Router, Response } from 'express';
import httpStatus from 'http-status';

export = ({ userRouter }): Router => {
    const router = Router();

    router.get('/', ( _, res: Response ) => {
        return res.status(httpStatus.OK).json({
            STATUS: 'OK',
        });
    });

    router.use('/users', userRouter);

    return router;
};
