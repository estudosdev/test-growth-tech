interface IServer {
    start(): Promise<any>
}

export default IServer;