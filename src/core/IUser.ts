export interface IUser {
    id: number
    name: string
    username: string
    address: {
        street: string
        suite: string
        city: string
        zipcode: string
    }
    phone: string
    website: string
    company: {
        name: string
        catchPhrase: string
        bs: string
    }
    posts?: Array<IPost>
}

export interface IPost {
    userId: number
    id: number
    title: string
    body: string
}